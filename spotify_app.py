#!/usr/bin/env python
#-*- encoding: utf-8 -*-


from gi.repository import Gtk, Gdk
from gi.repository.GdkPixbuf import Pixbuf


class Application():	
	def __init__(self):
		#Builder
		self.builder = Gtk.Builder()
		self.builder.add_from_file('spotify_app.glade')
		
		#Objetos
		self.window1 = self.builder.get_object('window1')
		self.image1 = self.builder.get_object('image1')
		self.button1  = self.builder.get_object('button1')
		self.button2 = self.builder.get_object('button2')
		self.image2 = self.builder.get_object('image2')
		self.label2 = self.builder.get_object('label2')
		self.label3 = self.builder.get_object('label3')
		self.entry1 = self.builder.get_object('entry1')
		self.entry2 = self.builder.get_object('entry2')
		self.button2 = self.builder.get_object('button2')
		
		#Nome
		self.label2.set_name('link1')
		self.label3.set_name('link2')
		self.entry2.set_name('ent_pass')
		self.button1.set_name('btn_login')
		self.button2.set_name('btn_facebook')
		self.entry1.set_name('ent_nome')
		
		#Configuração
		self.pb = Pixbuf.new_from_file_at_size('logo.png', 150,150)
		self.image1.set_from_pixbuf(self.pb)
		
		self.pb2 = Pixbuf.new_from_file_at_size('facebook.png', 25,25)
		self.image2.set_from_pixbuf(self.pb2)
		self.label2.set_label('')
		mk_lb2 = "<a href='https://www.spotify.com/br/password-reset/?utm_source=spotify&amp;utm_medium=login_box&amp;utm_campaign=forgot_password'>Forgot your password?</a>"
		self.label2.set_markup(mk_lb2)
		mk_lb3 = "Not a user? <a href='https://www.spotify.com/br/signup/'>Sing up</a>"
		self.label3.set_markup(mk_lb3)
		self.entry1.modify_fg(Gtk.StateType.NORMAL,Gdk.color_parse("#666666"))
		self.entry2.modify_fg(Gtk.StateType.NORMAL,Gdk.color_parse("#666666"))
		
		#Conexões
		self.button1.connect("clicked", self.on_button1_clicked)
		self.button2.connect("clicked", self.on_button2_clicked)
		
		#CSS
		css = """
			@import url("style.css");

			GtkWindow {
				background-color:#111111;
			}

		"""
		self.stylecssprovider = Gtk.CssProvider()
		self.stylecssprovider.load_from_data(css)
		Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), self.stylecssprovider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

		self.window1.connect('delete-event', Gtk.main_quit)
		self.window1.show_all()

	def on_button2_clicked(self, widget):
			print("%s clicked" % widget.get_name())

	def on_button1_clicked(self, widget):
		print("%s clicked" % widget.get_name())



if __name__ == "__main__":
	app = Application()
	Gtk.main()
